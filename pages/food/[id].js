import Layout from '../../components/layout';
import { getAllMenuItemIds, getMenuItemData } from '../../lib/food'; // Assuming you have these functions implemented
import React from 'react';

export async function getStaticPaths() {
    const paths = getAllMenuItemIds();
    return {
        paths,
        fallback: false,
    };
}

export async function getStaticProps({ params }) {
    const menuItemData = await getMenuItemData(params.id);

    return {
        props: {
            menuItemData,
        },
    };
}
//TODO: Doplnit AIkem více souboru v složce food, třeba pizza.md
//TODO: Upravit si layout v id.js
export default function MenuItem({ menuItemData }) {
    return (
        <Layout>
            <h1>{menuItemData.title}</h1>
            <p>ID: {menuItemData.id}</p>
            <p>Date: {menuItemData.date}</p>
            <div dangerouslySetInnerHTML={{ __html: menuItemData.contentHtml }} />
        </Layout>
    );
}
