import Head from 'next/head';
import React from 'react';
import Layout, { siteTitle } from '../components/layout';
import utilStyles from '../styles/utils.module.css';
import { getAllMenuItemsData } from '../lib/food';

export default function Home({ allPostsData }) {
    return (
        <Layout home>
            <Head>
                <title>{siteTitle}</title>
            </Head>
            <section className={`${utilStyles.headingMd} ${utilStyles.padding1px}`}>
                <ul className={utilStyles.list}>
                    {allPostsData.map(({ id, title }) => (
                        <li className={utilStyles.listItem} key={id}>
                            <span onClick={() => (window.location.href = `/food/${id}`)}>{title}</span>
                            <br />
                        </li>
                    ))}
                </ul>
            </section>
        </Layout>
    );
}

export async function getStaticProps() {
    const allPostsData = getAllMenuItemsData();
    return {
        props: {
            allPostsData,
        },
    };
}
