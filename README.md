# Závěrečná práce

## Jazyky a nástroje

- JavaScript
- React
- Node.js
- npm
## Funkce projektu

Tento projekt je webová aplikace, která zobrazuje menu pizzy a nápojů. Uživatelé mohou procházet různé druhy pizz a nápojů, které jsou k dispozici. Každá položka v menu má svůj vlastní detailní popis a je propojena s její vlastní stránkou.

Při kliknutí na název položky v menu je uživatel přesměrován na stránku s detaily této položky. Stránky s detaily položek jsou generovány staticky pomocí dat z markdown souborů v adresáři `food`.

Projekt je postaven na JavaScriptu a využívá React pro frontend a Node.js pro backend. Pro správu závislostí se používá npm.

## Jak spustit projekt

1. Nainstalujte závislosti pomocí `npm install`.
2. Spusťte projekt pomocí `npm run dev`.

## Testování

Projekt používá nástroje ESLint, markdownlint a Istanbul (nyc) pro kontrolu kódu a testování. Tyto nástroje můžete spustit pomocí následujících příkazů:

- `npm run lint`: Spustí ESLint na všech souborech JavaScript v projektu.
- `npm run markdownlint`: Spustí markdownlint na všech souborech Markdown v projektu.
- `npm run test`: Spustí testy a generuje zprávu o pokrytí kódu.

## Aktualizace a bezpečnost

Projekt používá `npm audit` pro kontrolu dostupnosti aktualizací a bezpečnostních aktualizací pro moduly. Tento nástroj můžete spustit pomocí příkazu `npm audit`.

## Nasazení

Nasazení je konfigurováno pomocí GitLab CI/CD. Podrobnosti naleznete v souboru `.gitlab-ci.yml`.