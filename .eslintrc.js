module.exports = {
    parser: '@typescript-eslint/parser',
    extends: ['plugin:prettier/recommended', 'plugin:react/recommended', 'plugin:@typescript-eslint/recommended'],
    plugins: ['@typescript-eslint'],
    root: true,
    parserOptions: {
        ecmaVersion: 2018,
        sourceType: 'module',
        ecmaFeatures: {
            jsx: true,
        },
    },
    rules: {
        'react-hooks/exhaustive-deps': 'off',
        'prettier/prettier': [
            'error',
            {
                semi: true,
                trailingComma: 'all',
                singleQuote: true,
                printWidth: 120,
                tabWidth: 4,
                endOfLine: 'auto',
            },
        ],
        'no-shadow': 'off',
        'react/prop-types': 'off',
        '@typescript-eslint/no-shadow': 'error',
        'no-use-before-define': 'off',
        '@typescript-eslint/no-use-before-define': ['error'],
        'import/prefer-default-export': 'off',
        '@typescript-eslint/no-inferrable-types': 'off',
        'class-methods-use-this': 'off',
        'no-useless-constructor': 'off',
        '@typescript-eslint/no-useless-constructor': 'error',
    },
    settings: {
        react: {
            version: 'detect',
        },
    },
    overrides: [
        {
            files: ['**/*.tsx'],
            excludedFiles: ['./src/types/api-schema.d.ts'],
            rules: {
                'react/prop-types': 'off',
            },
        },
    ],
    ignorePatterns: ['schema.ts'],
};
