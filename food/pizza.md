---
title: 'Pizza Menu'
date: '2023-05-01'
id: 'pizza'
---

# Pizzas

Welcome to our pizza menu. We offer a variety of delicious pizzas.

1. **Margherita**
   Description: Classic pizza with tomato sauce, mozzarella, fresh basil, salt, and extra-virgin olive oil.
   
2. **Pepperoni**
   Description: Tomato sauce, mozzarella, and pepperoni.

3. **BBQ Chicken**
   Description: BBQ sauce, mozzarella, chicken, onions, and cilantro.

4. **Hawaiian**
   Description: Tomato sauce, mozzarella, ham, and pineapple.

5. **Veggie**
   Description: Tomato sauce, mozzarella, bell peppers, onions, mushrooms, and olives.

6. **Meat Lovers**
   Description: Tomato sauce, mozzarella, pepperoni, sausage, ham, and bacon.

7. **Buffalo Chicken**
   Description: Buffalo sauce, mozzarella, chicken, and blue cheese.

8. **Mushroom**
   Description: Tomato sauce, mozzarella, and mushrooms.

9. **Four Cheese**
   Description: Tomato sauce, mozzarella, gorgonzola, parmesan, and ricotta.

10. **Seafood**
    Description: Tomato sauce, mozzarella, shrimp, calamari, and clams.