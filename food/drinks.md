---
title: 'Drinks Menu'
date: '2024-06-16'
---

## Cocktails

1. **Mojito**
   Description: A refreshing blend of white rum, fresh mint, lime juice, sugar, and soda water.
   Price: $8.99

2. **Cosmopolitan**
   Description: Vodka, triple sec, cranberry juice, and freshly squeezed lime juice.
   Price: $9.99

## Mocktails

3. **Virgin Pina Colada**
   Description: A tropical mix of coconut cream, pineapple juice, and crushed ice.
   Price: $6.99

4. **Berry Sparkler**
   Description: Fresh berries muddled with lemon juice, elderflower syrup, and soda water.
   Price: $5.99

## Beers

5. **IPA (India Pale Ale)**
   Description: Hop-forward with a strong malt backbone, notes of citrus, and a bitter finish.
   Price: $5.99

6. **Stout**
   Description: Rich and creamy with roasted malt flavors and hints of chocolate and coffee.
   Price: $6.99

## Wines

7. **Sauvignon Blanc**
   Description: Crisp and refreshing with notes of grapefruit, gooseberry, and tropical fruits.
   Price: $7.99 (glass) / $29.99 (bottle)

8. **Merlot**
   Description: Smooth and velvety with flavors of black cherry, plum, and a hint of vanilla.
   Price: $8.99 (glass) / $34.99 (bottle)
